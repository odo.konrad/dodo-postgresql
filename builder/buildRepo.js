const _ = require('lodash');
const { insertSql, allSql } = require('./sqlBuilder');
const { countSql } = require('./sqlBuilder');
const { listSql } = require('./sqlBuilder');
const { getSql } = require('./sqlBuilder');
const { updateSql } = require('./sqlBuilder');
const { buildWhere, prepareWhereData } = require('./buildWhere');

const buildRepoAction = (db, repo_config) => {
  const { schema, table } = repo_config;

  const queryMany = async (sql, data) => {
    try {
      console.log('SQL: ', sql);
      console.log('VALUES', data);
      return db.manyOrNone(sql, data);
    } catch (e) {
      console.error(e);
      throw { error: 'sql error' };
    }
  };

  const queryOne = async (sql, data) => {
    try {
      console.log('SQL: ', sql);
      return db.oneOrNone(sql, data);
    } catch (e) {
      console.error(e);
      throw { error: 'sql error' };
    }
  };

  const getFilterOption = (custom_filter = {}) => ({ ...repo_config.default_filter, ...custom_filter });

  const create = async (data, returning) => {
    if (!Object.keys(data).length) throw { error: 'Update SQL [data] cannot be empty' };
    const omit_data = _.pick(data, repo_config.create || []);
    const sql = insertSql(schema, table, omit_data, returning);
    console.log(sql, omit_data);
    return queryOne(sql, omit_data);
  };

  const update = async (where = {}, data = {}, settings = {}) => {
    const { custom_filter, all_item = false } = settings;
    if (!Object.keys(where).length) throw { error: 'Update SQL [where] cannot be empty' };
    if (!Object.keys(data).length) throw { error: 'Update SQL [data] cannot be empty' };
    const omit_where = _.pick(where, repo_config.update || []);
    const omit_data = _.pick(data, repo_config.update || []);
    const where_sql = buildWhere(omit_where, getFilterOption(custom_filter));
    const where_data = prepareWhereData(omit_data, getFilterOption(custom_filter));
    const sql = updateSql(schema, table, omit_data, where_sql);
    console.log({ ...where_data, ...omit_data });
    return queryOne(sql, { ...omit_where, ...omit_data });
  };

  const get = async (where, settings = {}) => {
    const { custom_filter } = settings;
    if (!Object.keys(where).length) throw { error: 'Update SQL [where] cannot be empty' };
    const omit_where = _.pick(where, repo_config.update || []);
    const where_sql = buildWhere(omit_where, getFilterOption(custom_filter));
    const where_data = prepareWhereData(where, getFilterOption(custom_filter));
    const sql = getSql(schema, table, where_sql);
    console.log({ sql, where_data });
    return queryOne(sql, where_data);
  };

  const getAll = async (where, settings = {}) => {
    const { custom_filter } = settings;
    if (!Object.keys(where).length) throw { error: 'Update SQL [where] cannot be empty' };
    const omit_where = _.pick(where, repo_config.update || []);
    const where_sql = buildWhere(omit_where, getFilterOption(custom_filter));
    const where_data = prepareWhereData(where, getFilterOption(custom_filter));
    const sql = allSql(schema, table, where_sql);
    console.log({ sql, where_data });
    return queryMany(sql, where_data);
  };

  const exist = async (where, settings) => {
    const result = await get(where, settings);

    return !!result;
  };

  const list = async (
    { filter = {}, limit = 10, order_by = {}, offset = 0 },
    settings = {},
  ) => {
    const { custom_filter, all_item = false } = settings;
    if (!order_by.field) order_by.field = 'id';
    if (!order_by.order) order_by.order = 'ASC';
    if ((!repo_config.list || []).find(key => key === order_by.field)) throw { error: `Cannot order by [${order_by.field}]` };

    const omit_where = _.pick(filter, repo_config.list || []);
    const where_sql = buildWhere(omit_where, getFilterOption(custom_filter));
    const where_data = prepareWhereData(omit_where, getFilterOption(custom_filter));

    if (all_item) {
      const sql = allSql(schema, table, where_sql);
      return queryMany(sql, { ...where_data });
    }

    const sql = listSql(schema, table, where_sql, order_by);
    const count_filtered = countSql(schema, table, where_sql);
    const count_total = countSql(schema, table);
    const item = await queryMany(sql, { ...where_data, limit, offset });

    const { count: filtered } = await queryOne(count_filtered, where_data);
    const { count: total } = await queryOne(count_total);

    return { item, filtered, total };
  };

  return {
    schema,
    table,
    db,
    queryMany,
    queryOne,
    create,
    update,
    get,
    getAll,
    list,
    exist,
  };
};

const buildRepo = (db) => (config) => buildRepoAction(db, config);

const buildView = (db) => (config) => ({
  ...buildRepoAction(db, config),
  create: undefined,
  delete: undefined,
  update: undefined,
});

module.exports = { buildRepo, buildView };
