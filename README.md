# dodo-postgresql

#### requirements

```angular2html
    Node: 16.13.1
```

## SETUP

```
yarn add dodo-postgresql@https://gitlab.com/odo.konrad/dodo-postgresql
```

### create file /setup/db.js

```javascript
const initDatabase = require('dodo-postgresql/init');
const buildRepo = require('dodo-postgresql/builder/buildRepo');

const config = {
    user: 'people',
    password: 'people',
    database: 'people',
};

const db = initDatabase(config.db);
const repo = buildRepo(db);

module.exports = {db, repo};
```

> Config -> in config you can add all properties from pg.promise

# BUILD REPO

> All table need repo file

### REPO EXAMPLE

```javascript
const {WHERE_TYPE} = require('dodo-postgresql/builder/buildWhere');
const {repo} = require('../../../setup/db');

const fields = [
    'name',
    'age',
];

const default_filter = {
    id: WHERE_TYPE.EQUAL,
    name: WHERE_TYPE.LIKE,
    age: WHERE_TYPE.EQUAL,
    created_at: WHERE_TYPE.BETWEEN_DATE,
};

const ExampleRepo = repo({
    schema: 'example_schema', // schema name
    table: 'example_table', // table name
    create: fields, //fields that restrict keys to insert
    update: ['id', ...fields], //fields that restrict keys to updates
    list: ['id', ...fields],  //fields that restrict keys to Select
    default_filter, // filter schema to build "where"
});

module.exports = AccountRepo;
```

### REPO INSERT

```javascript
const body = {
    name: 'Test',
    age: 25,
}

const id = await ExampleRepo.create(body)
```

> create function had 2 parameters
> 1. object with data
> 2. returning -> default is 'id', its the value return from insert sql

### REPO Update

```javascript
const body = {
    name: 'Test',
    age: 25,
}

const where = {id: 1};

await ExampleRepo.update(where, body);
```

> update function had **2 parameters**
> 1. object with where properties
> 2. object with data to update
> 3. settings -> more in *Repo Settings*

### REPO GET

```javascript
const where = {id: 1};

await ExampleRepo.get(where);
```

> update function had **2 parameters**
> 1. object with where properties
> 2. settings -> more in *Repo Settings*
>
> get returning always 1 element


> update function had **3 parameters**
> 1. object with where properties
> 2. object with data to update
> 3. settings -> more in *Repo Settings*

### REPO GET ALL

```javascript
const where = {name: '%józek%'};

await ExampleRepo.getAll(where);
```

> update function had **2 parameters**
> 1. object with where properties
> 2. settings -> more in *Repo Settings*

### REPO LIST

```javascript
const opt = {
    filter: {
        name: '%józek%'
    }, //default: empty object
    limit: 10, //default: '10'
    order_by: {
        field: 'id', // default: 'id'
        order: 'ASC', //default: 'ASC'
    },
    offset: 0, //default: 'offset'
};

await ExampleRepo.list(opt);
```

> update function had **2 parameters**
> 1. object option to create page of list
> 2. settings -> more in *Repo Settings*

> Function return object with 3 parameters
> 1. item -> list of all element
> 2. filtered -> number of all element id table with current filter
> 3. total -> number of all element in table

### REPO EXIST

```javascript
const where = {id: 1};

await ExampleRepo.exist(where);
```

> update function had **2 parameters**
> 1. object with where properties
> 2. settings -> more in *Repo Settings*

### REPO QUERY ONE

```javascript
const sql = 'SELECT * FROM example.example WHERE id=$[id]'
const data = {id: 1}

await ExampleRepo.queryOne(sql, data);
```

> update function had **2 parameters**
> 1. String with sql string
> 2. Object with data

> ***WARNING -> this query return error when find more than 1 element***

### REPO QUERY MANY

```javascript
const sql = 'SELECT * FROM example.example WHERE id=$[id]'
const data = {id: 1}

await ExampleRepo.queryMany(sql, data);
```

> update function had **2 parameters**
> 1. String with sql string
> 2. Object with data

### Repo WHERE BUILD

> Where is built on the basis of the default_filter key
> example

```javascript
const default_filter = {
    id: WHERE_TYPE.IN,
    name: WHERE_TYPE.LIKE,
    age: WHERE_TYPE.EQUAL,
};
```

### Repo Custom method

```javascript
const ExampleRepo = repo({
    ...
});

ExampleRepo.customMethod = async (id, name) => {
    if (await ExampleRepo.exist({id})) {
        await ExampleRepo.update({id}, {name})
    }
}

module.exports = AccountRepo;
```

> if you put for **where = { id: [2, 1], name: '%juzek', age: 25 }**
> then builder return
>
> `WHERE id IN (2,1) AND name ILIKE %juzek AND age = 25`

### Repo Settings PARAMETERS

```javascript

const settings = {
    custom_filter: {id: WHERE_TYPE.IN}, // custom filters can ovveride default filter from Repo
    all_item: true // remove Limit From SQL
}

await ExampleRepo.list(opt, settings);
```

### Repo Transaction

// TODO

# Postgresql session

### Session setup

```sql
create table session
(
    token   varchar(255),
    user_id integer not null,
    "user"  jsonb,
    role    integer
);
```

> In `/setup/auth.js`

```javascript
const authorization = require('dodo-postgresql/session/pg_session');
const {db} = require('./db');

const {checkSession, createSession, deleteSession} = authorization(db);

module.exports = {createSession, checkSession, deleteSession};
```

### create session

> create session require 3 parameters,
> 2. id -> (integer) id of user
> 3. role -> (integer) enum of roles -> opcional
> 4. user -> Object with user data
>
> function return token of session

### check session

> checkSession 1 parameters,
> 1. token -> (string or null)
>
> function return record of session or null

### delete session

> deleteSession 1 parameters,
> 1. token -> (string or null)

# Migration

> create file ./migration.js

 ```javascript
const migration_script = require('dodo-postgresql/migration');
const {db} = require('./setup/db');

migration_script(db);
```

> add this script to the scripts in package.json
>
>"migration-create": "node migration.js --create",
>
>"migration-run": "node migration.js --run"

// TODO
