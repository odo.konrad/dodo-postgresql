const pgp = require('pg-promise')();

const init = (db_config = {}) => {
    try {
        const connection = pgp({
            user: db_config.user,
            password: db_config.password,
            host: db_config.host || 'localhost',
            database: db_config.database,
            port: db_config.port || 5432,
        })
        
        return connection;
    } catch (e) {
        console.error('connection not working')
    }

}

module.exports = init;
