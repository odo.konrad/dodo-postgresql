const { queryField } = require('./sqlHelper');

const WHERE_TYPE = {
  EQUAL: 1,
  LIKE: 2,
  IN: 3,
  BETWEEN_DATE: 4,
  MORE: 5,
  MORE_OR_EQUAL: 6,
  LESS: 7,
  LESS_OR_EQUAL: 8,
};

const buildEqual = (field) => `${field} = ${queryField(field)}`;

const buildLike = (field) => `${field} ILIKE ${queryField(field)}`;

const buildIn = (field) => `${field} IN (${queryField(`${field}:csv`)})`;

const buildBetweenDate = (field) => `${field} between $[${field}_from] and $[${field}_to]`;

const buildMore = (field) => `${field} > ${queryField(field)}`;

const buildMoreOrEqual = (field) => `${field} >= ${queryField(field)}`;

const buildLess = (field) => `${field} < ${queryField(field)}`;

const buildLessOrEqual = (field) => `${field} <= ${queryField(field)}`;

const buildWhere = (data, where_config) => {
  const field_list = Object.keys(data);
  const where = field_list.map(field => {
    const where_type = where_config[field];

    switch (where_type) {
      case WHERE_TYPE.EQUAL: {
        return buildEqual(field);
      }
      case WHERE_TYPE.LIKE: {
        return buildLike(field);
      }
      case WHERE_TYPE.IN: {
        return buildIn(field);
      }
      case WHERE_TYPE.BETWEEN_DATE: {
        return buildBetweenDate(field);
      }
      case WHERE_TYPE.MORE: {
        return buildMore(field);
      }
      case WHERE_TYPE.MORE_OR_EQUAL: {
        return buildMoreOrEqual(field);
      }
      case WHERE_TYPE.LESS: {
        return buildLess(field);
      }
      case WHERE_TYPE.LESS_OR_EQUAL: {
        return buildLessOrEqual(field);
      }
      default: {
        return '';
      }
    }
  });

  return where.length ? `WHERE ${where.join(' AND ')}` : '';
};

const prepareWhereData = (data, where_config) => {
  const newData = {};
  Object.entries(data).forEach(([field, value]) => {
    const where_type = where_config[field];

    switch (where_type) {
      case WHERE_TYPE.EQUAL: {
        newData[field] = value;
        break;
      }
      case WHERE_TYPE.LIKE: {
        newData[field] = value;
        break;
      }
      case WHERE_TYPE.IN: {
        newData[field] = value;
        break;
      }
      case WHERE_TYPE.BETWEEN_DATE: {
        newData[`${field}_from`] = value.from;
        newData[`${field}_to`] = value.to;
        break;
      }
      default: {
        break;
      }
    }
  });

  return newData;
};

module.exports = { WHERE_TYPE, buildWhere, prepareWhereData };
