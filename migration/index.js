const { create } = require('./create.js');
const { runMigration } = require('./run.js');

const repo = async (db) => {
    try {
        await db.query('SELECT * FROM migration');
        return true;
    } catch (e) {
        if (e.code = '42P01') {
            db.query(
                `create table migration
                    (
                        id         serial
                            constraint migration_pk
                                primary key,
                                uid varchar(255),
                        name       varchar(255),
                        created_at timestamp default now(),
                        status     varchar(255),
                        message    jsonb
                    );

                    alter table migration
                        owner to people;`
            )

            return true;
        } else {
            throw 'MIGRATION IS FAILED'
        }
    }
}

const run = async (db) => {
    const myArgs = process.argv.slice(2);
    const option = myArgs[0];

    switch (option) {
        case '--create': {
            const name = myArgs[1];
            create(name)
            break;
        }

        case '--run': {
            await repo(db)
            await runMigration(db);
            break;
        }

        default: {
            console.log('UNKNOWN OPTION ----> ' + option)
            break;
        }
    }
}

module.exports = run;
