const { randomBytes } = require('crypto');

const authorization = (db) => ({
  createSession: async (data) => {
    if (!data) throw { message: 'JWT: data cannot be empty' };
    const token = randomBytes(50).toString('hex');
    const { id, role = null, ...user } = data;
    const existed_user = await db.oneOrNone('SELECT * FROM session WHERE user_id = $[id]', { id });
    if (existed_user) {
      return existed_user?.token;
    }
    await db.query('INSERT INTO session (token, user_id, role, "user") VALUES ($[token], $[id], $[role], $[user])', {
      token, id, role, user,
    });
    return token;
  },
  checkSession: async (token) => {
    try {
      const res = await db.oneOrNone('SELECT * FROM session WHERE token = $[token]', { token });
      if (!res) return null;
      return res;
    } catch (e) {
      return null;
    }
  },
  deleteSession: async (token) => {
    await db.oneOrNone('DELETE FROM session WHERE token = $[token]', { token });
  },
});

module.exports = authorization;
