const fs = require('fs');
const path = require('path');

const readAllMigrationFile = () => {
  const folder_list = fs.readdirSync(path.join(process.cwd(), 'migration'));
  return folder_list.map(folder => {
    const p = path.join(process.cwd(), 'migration', folder, 'run.json');
    const json = fs.readFileSync(p, { encoding: 'utf-8' });
    return JSON.parse(json);
  });
};

const runMigration = async (db) => {
  const migration_old = await db.query('SELECT * FROM migration WHERE status = \'DONE\'');
  const migration_all = readAllMigrationFile();
  const migration_to_run = migration_all.filter(migration => !migration_old.some(old => old.uid === migration.uid));
  const sorted_by_date = migration_to_run.sort((prev, next) => new Date(prev.date) - new Date(next.date));

  for (const migration of sorted_by_date) {
    try {
      for (const m of migration.migration) {
        const p = path.join(process.cwd(), 'migration', `${new Date(migration.date) / 1}-${migration.name}-${migration.uid}`, m);
        if (m.includes('.sql')) {
          const file = fs.readFileSync(p, { encoding: 'utf-8' });
          await db.query(file);
        }

        if (m.includes('.js')) {
          await require(p).run();
        }
      }
      await db.query('INSERT INTO migration (uid, name, status, message) VALUES ($[uid], $[name], $[status], $[message]);',
        { uid: migration.uid, name: migration.name, status: 'DONE', message: JSON.stringify(migration) });
    } catch (e) {
      console.log(e);
      await db.query('INSERT INTO migration (uid, name, status, message) VALUES ($[uid], $[name], $[status], $[message]);',
        { uid: migration.uid, name: migration.name, status: 'ERROR', message: JSON.stringify(e) });
      throw 'EROOR ON MIGRATION';
    }
  }
};

module.exports = { runMigration };
