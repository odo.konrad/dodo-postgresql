const { queryField } = require('./sqlHelper');

const insertSql = (schema, table, data, returning = 'id') => {
  const record_name = Object.keys(data).map(el => `"${el}"`);
  const values = Object.keys(data).map(field => queryField(field)).join(', ');

  return `INSERT INTO ${schema}.${table} (${record_name.join(', ')})
            VALUES (${values})
            RETURNING ${returning}`;
};

const updateSql = (schema, table, data, where, returning = 'id') => {
  const record_name = Object.keys(data);
  const values = record_name.map(field => `"${field}" = ${queryField(field)}`).join(', ');

  return `UPDATE ${schema}.${table}
            SET ${values} ${where}
            RETURNING ${returning}`;
};

const getSql = (schema, table, where) => `SELECT *
                                          FROM ${schema}.${table} ${where}
                                          LIMIT 1`;

const allSql = (schema, table, where) => `SELECT *
                                          FROM ${schema}.${table} ${where}`;

const listSql = (schema, table, where, order_by) => `SELECT *
                                                     FROM ${schema}.${table} ${where}
                                                     ORDER BY "${order_by.field}" ${order_by.order}
                                                     LIMIT ${queryField('limit')} OFFSET ${queryField('offset')}`;

const countSql = (schema, table, where) => `SELECT count(*) ::integer
                                            FROM ${schema}.${table} ${where}`;

module.exports = { insertSql, updateSql, getSql, listSql, countSql, allSql };
