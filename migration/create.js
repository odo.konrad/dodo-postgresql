const fs = require('fs');
const path = require('path');
const shell = require('shelljs');
const { v4 } = require('uuid');

const script = (name, id, date) => `{
    "name": "${name}",   
    "uid": "${id}",
    "migration": ["script.sql", "script.js"],
    "date": "${date}"    
}`;

const script_sql = '-- CREATE MIGRATION';

const script_js = `
const run = async () => {
    //TODO CREATE MIGRATION
}

module.exports = { run }
`;
const create = (name) => {
  const id = v4();
  const date = new Date().toISOString();
  const migration_name = `${new Date(date) / 1}-${name}-${id}`;
  const path_to_migration = path.join(process.cwd(), 'migration', migration_name);
  shell.mkdir('-p', path_to_migration);
  fs.writeFileSync(path.join(path_to_migration, 'run.json'), script(name, id, date));
  fs.writeFileSync(path.join(path_to_migration, 'script.sql'), script_sql);
  fs.writeFileSync(path.join(path_to_migration, 'script.js'), script_js);
};

module.exports = { create };
